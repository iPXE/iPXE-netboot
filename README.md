[![build status](https://gitlab.com/iPXE/iPXE-netboot/badges/master/build.svg)](https://gitlab.com/iPXE/iPXE-netboot/commits/master)

Création d'un environnement de déploiement unifié, multi OS, multi plateformes.

Ce projet permettra le déploiement d'installations linux, de Captures/restaurations de windows, ...

Certains autres projets seront utilisés dont : 
- ipxe.org
- apache
- Outils de déploiement Windows
- Powershell
- ...(Complèté petit à petit)