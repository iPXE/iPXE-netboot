﻿# ---------------------------------------------------------------------------
# Purpose: Used to create a WinPE boot image
# Version: 1.2 - February 26, 2017 - Fabian Clerbois
# 
# This script is provided "AS IS" with no warranties, confers no rights and 
# is not supported by the authors or Deployment Artist. 
#
# ---------------------------------------------------------------------------

# Chck for parameter
param(
	[string]$WinPE_Architecture
)
# Check for elevation
If (-NOT ([Security.Principal.WindowsPrincipal] [Security.Principal.WindowsIdentity]::GetCurrent()).IsInRole(`
    [Security.Principal.WindowsBuiltInRole] "Administrator"))
{
    Write-Warning "Oupps, you need to run this script from an elevated PowerShell prompt!`nPlease start the PowerShell prompt as an Administrator and re-run the script."
	Write-Warning "Aborting script..."
    Break
}


# Functions
Function Get-FolderName($initialDirectory, $Description)
{
	Write-Host "Select $Description"
	Write-Host $initialDirectory
	Add-Type -AssemblyName System.Windows.Forms
	$FolderBrowser = New-Object System.Windows.Forms.FolderBrowserDialog -Property @{
		SelectedPath=$initialDirectory.trim()
	}
	$FolderBrowser.Description = $Description
    $FolderBrowser.ShowDialog() | Out-Null
	if($?)
	{
	   return $FolderBrowser.SelectedPath
	}
	else
	{
	   return "None"
	}
}
$scriptPath = split-path -parent $MyInvocation.MyCommand.Definition


# Core Settings
$WinPE_BuildFolder = "$env:temp\WinPE"
$WinPE_ExtraFiles =Get-FolderName $env:temp "ExtraFiles"
if ($WinPE_Architecture -ne "x86")
{
	$WinPE_Architecture = "amd64" # Or x86
}
$WinPE_Drivers =Get-FolderName $WinPE_ExtraFiles "Drivers"  # Read-Host -Prompt 'WinPE Drivers path'
$WinPE_MountFolder = Get-FolderName $WinPE_Drivers "Mount$WinPE_Architecture"
if ($WinPE_MountFolder -eq "None")
{
	$WinPE_MountFolder = join-path $env:temp "Mount$WinPE_Architecture"
	New-Item $WinPE_MountFolder -type directory
}
$WinPE_language = "fr-FR"
$Adk_Folder = "C:\Program Files (x86)\Windows Kits\10"
while (!(Test-Path  $Adk_Folder))
{
	$Adk_Folder = Get-FolderName $WinPE_Drivers "AdkFolder"
}
$WinPE_Locale = (Get-WinUserLanguageList)[0].LanguageTag


# Delete existing WinPE build folder (if exist)
try 
{
if (Test-Path -path $WinPE_BuildFolder) {Remove-Item -Path $WinPE_BuildFolder -Recurse -ErrorAction Stop}
}
catch
{
    Write-Warning "Oupps, Error: $($_.Exception.Message)"
    Write-Warning "Most common reason is existing WIM still mounted, use ImageX /unmount to clean up and run script again"
    Break
}

# Set paths to WinPE folders and tools
$ADK_Path = "$Adk_Folder\Assessment and Deployment Kit"
$WinPE_ADK_Path = $ADK_Path + "\Windows Preinstallation Environment"
$WinPE_OCs_Path = $WinPE_ADK_Path + "\$WinPE_Architecture\WinPE_OCs"
$DISM_Path = $ADK_Path + "\Deployment Tools" + "\$WinPE_Architecture\DISM"

# Make a copy of the WinPE boot image from ADK
if (!(Test-Path -path $WinPE_BuildFolder)) {New-Item $WinPE_BuildFolder -Type Directory}
Copy-Item "$WinPE_ADK_Path\$WinPE_Architecture\en-us\winpe.wim" "$WinPE_BuildFolder\winpe$WinPE_Architecture.wim"

# Mount the WinPE image
$WimFile = "$WinPE_BuildFolder\winpe" + $WinPE_Architecture + ".wim"
write-host $WimFile
Mount-WindowsImage -ImagePath $WimFile -Path $WinPE_MountFolder -Index 1;

# Copy WinPE ExtraFiles
if ($WinPE_ExtraFiles -ne "None")
{
	Copy-Item $WinPE_ExtraFiles\* $WinPE_MountFolder -Recurse
}

# Copy new Background image
Copy-Item "$scriptPath\winpe.jpg" "$WinPE_MountFolder\windows\system32\winpe.jpg"

# Add startnet.cmd from repository
Copy-Item "$scriptPath\startnet.cmd" "$WinPE_MountFolder\windows\system32\startnet.cmd"


# Add WinPE optional components
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\WinPE-WinReCfg.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\$WinPE_language\WinPE-WinReCfg_$WinPE_language.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\WinPE-Scripting.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\$WinPE_language\WinPE-Scripting_$WinPE_language.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\WinPE-WMI.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\$WinPE_language\WinPE-WMI_$WinPE_language.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\WinPE-NetFx.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\$WinPE_language\WinPE-NetFx_$WinPE_language.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\WinPE-PowerShell.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\$WinPE_language\WinPE-PowerShell_$WinPE_language.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\WinPE-DismCmdlets.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\$WinPE_language\WinPE-DismCmdlets_$WinPE_language.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\WinPE-SecureBootCmdlets.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\$WinPE_language\WinPE-SecureBootCmdlets_$WinPE_language.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\WinPE-StorageWMI.cab
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Package /PackagePath:$WinPE_OCs_Path\$WinPE_language\WinPE-StorageWMI_$WinPE_language.cab

# Set Language
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Set-SysLocale:$WinPE_Locale
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Set-UserLocale:$WinPE_Locale
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Set-InputLocale:$WinPE_Locale

# Install WinPE 5.0 Drivers (using ADK 8.1 version of dism.exe instead of Add-WindowsDriver)
& $DISM_Path\dism.exe /Image:$WinPE_MountFolder /Add-Driver /Driver:$WinPE_Drivers /Recurse

# Unmount the WinPE image and save changes
Dismount-WindowsImage -Path $WinPE_MountFolder -Save