@echo off
wpeinit
:TRYAGAIN
ECHO Network connection test, please wait...
PING -n 1 ipxe |find "Reply from " >NUL
IF NOT ERRORLEVEL 1 goto :SUCCESS
IF     ERRORLEVEL 1 goto :TRYAGAIN

:SUCCESS
echo Network OK, Continuing
net use z: \\172.20.0.44\scriptspe$
z:
script.bat
